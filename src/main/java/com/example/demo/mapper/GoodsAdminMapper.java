package com.example.demo.mapper;

import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.common.CommonResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsAdminMapper {
    List<Goods> selectAllGoods();

    List<Goods> selectByPage(@Param("page") Integer page, @Param("size") Integer size);
    Integer selectCount();

    Goods selectGoodsById(@Param("goodsId") String id);

    int updateById(Goods goods);
}
