package com.example.demo.mapper;

import com.example.demo.pojo.DO.Goods;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Entity com.example.demo.pojo.DO.Goods
 */
public interface GoodsMapper extends BaseMapper<Goods> {

    List<Goods> selectGoodsByCategory(@Param("name") String categoryName);

    List<Goods> selectGoodsByGoodsName(@Param("name") String goodsName);

    void insertImg(@Param("goodsId") String goodsId, @Param("imgUrl") String s);

    List<Goods> selectGoodsByCategoryId(@Param("categoryId") String categoryId);
}




