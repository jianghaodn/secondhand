package com.example.demo.mapper;

import com.example.demo.pojo.DO.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 */
public interface UserAdminMapper {
    void updateUserStatus(@Param("user") User user);

    List<User> selectByPage(@Param("page") Integer page,@Param("size") Integer size);

    int selectCount();

    List<User> selectUserByName(@Param("name") String name);
}
