package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.DO.Category;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Administrator
 * @Entity com.team.springboot.pojo.DO.Category
 */
public interface CategoryMapper extends BaseMapper<Category> {


    Category selectByIdAdmin(@Param("categoryId") String id);

    List<Category> selectAll();
}
