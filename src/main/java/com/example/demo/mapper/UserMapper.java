package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.DO.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 * @Entity com.team.springboot.pojo.DO.User
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


}
