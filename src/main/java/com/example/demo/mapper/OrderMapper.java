package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.DO.Order;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Administrator
 * @Entity com.team.springboot.pojo.DO.Order
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {


}
