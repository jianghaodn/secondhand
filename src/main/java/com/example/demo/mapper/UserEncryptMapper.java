package com.example.demo.mapper;

import com.example.demo.pojo.DO.UserEncrypt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author dahaozi
* @description 针对表【user_encrypt(保存用户密钥)】的数据库操作Mapper
* @createDate 2022-12-03 20:27:08
* @Entity com.example.demo.pojo.DO.UserEncrypt
*/
public interface UserEncryptMapper extends BaseMapper<UserEncrypt> {

}




