package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.DO.ShoppingCar;

/**
 * @author Administrator
 * @Entity com.team.springboot.pojo.DO.ShoppingCar
 */
public interface ShoppingCarMapper extends BaseMapper<ShoppingCar> {


}
