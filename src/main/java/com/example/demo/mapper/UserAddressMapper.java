package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.pojo.DO.UserAddress;
import org.apache.ibatis.annotations.Param;

/**
 * @author Administrator
 * @Entity com.team.springboot.pojo.DO.UserAddress
 */
public interface UserAddressMapper extends BaseMapper<UserAddress> {


    UserAddress select(@Param("ua") UserAddress userAddress);

}
