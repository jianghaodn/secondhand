package com.example.demo.configuration;

import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.utils.JwtUtil;
import com.example.demo.utils.LogUtils;
import com.example.demo.utils.WebUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.*;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;


/**
 * @author Administrator
 */
@Configuration
@Slf4j
public class MvcConfig implements WebMvcConfigurer {

    @Resource
    private LoginInterceptor loginInterceptor;

    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                .allowCredentials(true)
                .allowedHeaders("*")
                .maxAge(3600);
    }

    private static final String[] CLASSPATH_RESOURCE_LOCATIONS = {
//            "classpath:/META-INF/resources/",
//            "classpath:/resources/",
            "classpath:/static/",
            "classpath:/public/"
    };


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }

    /**
     * 使用@Bean注入fastJsonHttpMessageConvert
     * 替换springboot默认的jackson，使用FastJson作为解析工具，将对象转化为json字符串传送到前台
     */
    @Bean
    public HttpMessageConverter fastJsonHttpMessageConverters() {
        //1.需要定义一个Convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");

        //设置FastJson的消息转换属性，如果此列属性为null，也要显示
        //如果不设置这个，属性为null的列会被过滤
        fastJsonConfig.setSerializerFeatures(SerializerFeature.WriteMapNullValue);

        SerializeConfig.globalInstance.put(Long.class, ToStringSerializer.instance);

        fastJsonConfig.setSerializeConfig(SerializeConfig.globalInstance);

        fastConverter.setFastJsonConfig(fastJsonConfig);
        HttpMessageConverter<?> converter = fastConverter;
        return fastConverter;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(fastJsonHttpMessageConverters());
    }
}

/**
 * 自定义登录拦截器
 */
@Slf4j
@Component
class LoginInterceptor implements HandlerInterceptor {

    @Resource
    private LogUtils logUtils;

    /**
     * 需要登录才能访问
     */
    private final String[] needLoginArr = {
            "/user/**", "/order/**", "/goods/buy", "/goods/user", "/goods/car"
    };

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object o) throws Exception {
        logUtils.log(request);

        //设置响应编码
        response.setCharacterEncoding("utf-8");
        String servletPath = request.getServletPath();
        String token = request.getHeader("token");
        log.info("请求路径：" + servletPath);
        //如果是登录或者注册的请求，直接放行
        String loginPath = "/user/login";
        String registerPath = "/user/register";
        //登录注册直接放行
        if (loginPath.contains(servletPath) || registerPath.contains(servletPath) || isResources(servletPath)) {
            return true;
        }
        if (servletPath.equals("/info")) {
            if (!StringUtils.hasText(token)) {
                response.sendRedirect(SystemConstant.ADMIN_LOGIN);
                System.out.println(servletPath+"拦截");
                return false;
            }
        }
        if (servletPath.contains(".html")) {
            return true;
        }
        if (needLogin(servletPath) && !StringUtils.hasText(token)) {
            //没有登录
            log.info("请登录！");
            if (servletPath.contains("/admin")){
                response.sendRedirect(SystemConstant.ADMIN_LOGIN);
            }else {
                WebUtils.render(response, CommonResult.result(HttpStatus.FORBIDDEN.value(), "请登录！"));
            }
            return false;
        } else {
            //需要登录且已经携带token
            //解析token
            if (StringUtils.hasText(token)) {
                try {
                    String subject = JwtUtil.parseJWT(token).getSubject();
                    request.setAttribute(SystemConstant.USER_ID, subject);
                    log.info("请求用户：" + subject);
                } catch (Exception e) {
                    //解析失败，向前端返回需要登录的信息
                    log.error(e.getMessage());
                    log.error("token解析失败");
                    WebUtils.render(response, CommonResult.result(HttpStatus.FORBIDDEN.value(), "请登录！"));
                    return false;
                }
            }
        }
        //不需要登录，直接放行
        return true;
    }

    /**
     * 验证路径是否为需要登录的路径
     *
     * @param servletPath
     * @return
     */
    private boolean needLogin(String servletPath) {
        return servletPath.contains("/admin") || servletPath.contains("/goods/buy")
                || servletPath.contains("/goods/car") || servletPath.contains("/user/")
                || servletPath.contains("/order") || servletPath.contains("/shoppingCar")
                || servletPath.contains("/goods/user");
    }

    /**
     * 检测请求是否是对资源的处理
     * @param servletPath 请求路径
     * @return
     */
    private Boolean isResources(String servletPath) {
        return servletPath.matches("^(/admin/Shimba/assets.*)|.*(\\.jpg|\\.png|\\.js|\\.css|\\.img|\\.ttf|\\.map|\\/error|\\.woff|\\.woff2)$");
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object o,
                           ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) throws Exception {

    }
}