package com.example.demo.controller;


import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.example.demo.common.SystemException;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author Administrator
 */
@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {

    /**
     * * 跳转到错误请求信息页面
     * * @return
     */
    @GetMapping("/test1")
    public String error() {
        System.out.println("发生错误了");
        return "error/404";
    }

    @GetMapping("/test2")
    public String error1() {
        System.out.println("发生错误了");
        return "/error/404";
    }

    @GetMapping("/user")
    @ResponseBody
    public String user(HttpServletRequest request) {
        int i = 10 / 0;
        return (String) request.getAttribute(SystemConstant.USER_ID);
    }


    @GetMapping("exception")
    public String exception() {
        throw new SystemException("手动抛出一个异常");
    }

    @GetMapping("/assert")
    public void assertTest(String name) {
        Assert.notNull(null, "不能为空");
        log.info("test");
    }

    public static String encrypt(String str) {
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();

        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        byte[] encrypt = aes.encrypt(StrUtil.bytes(str));
        return ArrayUtil.join(encrypt, "");
    }

    @PostMapping("/file")
    public CommonResult uploadFile(HttpServletRequest request) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        List<MultipartFile> file =
                multipartRequest.getFiles("file");
        System.out.println(file);
        String goodsId = request.getParameter("goodsId");
        System.out.println(goodsId);
        return CommonResult.success();
    }

    @PostMapping("/file_2")
    public CommonResult uploadFile_2(MultipartFile[] file, Goods goods) {
        System.out.println(file.length);
        System.out.println(goods);
        return CommonResult.success();
    }
}

