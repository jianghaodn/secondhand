package com.example.demo.controller;

import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.DO.UserAddress;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public CommonResult login(@RequestBody User user) {
        return userService.login(user);
    }

    @PostMapping("/register")
    public CommonResult register(@RequestBody User user) {
        return userService.register(user);
    }

    @GetMapping("/info")
    public CommonResult getUserInfo(HttpServletRequest request) {
        String userId = (String) request.getAttribute("userId");
        if (userId == null){
            return CommonResult.error("需要登录！");
        }
        //解析token
        return userService.getUserInfo(userId);
    }

    @PutMapping("/updateUser")
    public CommonResult updateUser(HttpServletRequest request,@RequestBody User user) {
        String userId= (String) request.getAttribute(SystemConstant.USER_ID);
        user.setId(userId);
        return userService.editUser(user);
    }

    @PutMapping("/updatePassword")
    public CommonResult updatePassword(@RequestBody User user) {
        return userService.editPassword(user);
    }

    @PostMapping("/logout")
    public CommonResult logout(HttpServletRequest request) {
        //如何让前端的token失效呢？
        //将token存入redis服务器。
        //用户手动退出的时候，将token从这个服务器中删除
        //每次需要验证token的时候，先从redis中验证，如果redis中已经不存在了，说明肯定是失效了

        //暂时只设置为前端删除token
        return CommonResult.okResult();
    }


    @PostMapping("/addUserAddress")
    public CommonResult addAddress(HttpServletRequest request,@RequestBody  UserAddress userAddress){
        userAddress.setUserId((String) request.getAttribute(SystemConstant.USER_ID));
        return userService.addAddress(userAddress);
    }

    @DeleteMapping("/deleteAddress")
    public CommonResult deleteAddress(HttpServletRequest request,@RequestBody  UserAddress userAddress){
        userAddress.setUserId((String) request.getAttribute(SystemConstant.USER_ID));
        return userService.removeAddress(userAddress);
    }

}
