package com.example.demo.controller;

import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.service.GoodsAdminService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin")
public class GoodsAdminController {

    @Resource
    private GoodsAdminService goodsAdminService;

    @GetMapping("/goods/all")
    @ApiOperation("获取数据，不分页")
    public CommonResult getGoodsList(){
        return goodsAdminService.getGoodsList(null,null);
    }

    @GetMapping("/goods/page")
    @ApiOperation("分页获取数据")
    public CommonResult getGoodsList(@ApiParam("第几页") Integer page, @ApiParam("分页大小") Integer size) {
        return goodsAdminService.getGoodsList(page,size);
    }
    @GetMapping("/goods/{id}")
    public CommonResult getGoodsInfoById(@PathVariable String id){
        return goodsAdminService.getGoodsInfoById(id);
    }

    @DeleteMapping
    public CommonResult removeGoods(@RequestBody Goods goods){
        return goodsAdminService.removeGoodsById(goods);
    }

    @PutMapping("/goods")
    public CommonResult modifyGoods(@RequestBody Goods goods) {
        return goodsAdminService.modifyGoods(goods);
    }
}
