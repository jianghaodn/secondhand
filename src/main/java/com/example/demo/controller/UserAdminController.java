package com.example.demo.controller;

import cn.hutool.http.server.HttpServerResponse;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.GoodsAdminService;
import com.example.demo.service.UserAdminService;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Administrator
 */
@Controller
@RequestMapping("/admin")
public class UserAdminController {

    @Resource
    private UserAdminService userAdminService;

    @Resource
    private GoodsAdminService goodsAdminService;
    @Resource
    private UserService userService;


    @GetMapping
    public String toAdmin() {
        return SystemConstant.INDEX;
    }

    @PostMapping("/user")
    @ResponseBody
    public CommonResult settingUser(@RequestBody User user) {
        return userAdminService.settingUser(user);
    }

    public CommonResult settingGoods() {
        return null;
    }

    public CommonResult getAllCount() {
        return null;
    }

    /**
     * 导出用户数据为excel
     *
     * @return 用户数据
     */
    @GetMapping("exportUserData")
    @ResponseBody
    public void exportUserData(HttpServletResponse response) {
        userAdminService.exportUserData(response);
    }

    @GetMapping("exportGoodsData")
    @ResponseBody
    public void exportGoodsData(HttpServletResponse response) {
        userAdminService.exportGoodsData(response);
    }

    @GetMapping("exportCategoryData")
    @ResponseBody
    public void exportCategoryData(HttpServletResponse response) {
        userAdminService.exportCategoryData(response);
    }

    public void exportOrdersData() {
    }

    @PostMapping("/userList")
    @ResponseBody
    public CommonResult getAllUsers() {
        return CommonResult.okResult(userService.list());
    }

    @PostMapping("/userListByPage")
    @ResponseBody
    public CommonResult getAllUsers(Integer page, Integer size) {
        return userAdminService.getUsersByPage(page, size);
    }


    @PostMapping("/searchUserByName")
    @ResponseBody
    public CommonResult getUserByName(String name) {
        return userAdminService.getUserByName(name);
    }

    @PutMapping("setUserStatus")
    @ResponseBody
    public CommonResult modifyUser(@RequestBody User user) {
        return userAdminService.setUserStatus(user);
    }


}
