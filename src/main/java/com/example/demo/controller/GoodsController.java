package com.example.demo.controller;


import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.GoodsService;
import com.example.demo.utils.SysUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/goods")
@Api(value = "商品模块")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping
    @ApiOperation("获取全部商品信息")
    public CommonResult getAllGoodsList() {
        return CommonResult.okResult(goodsService.list());
    }

    @GetMapping("id")
    @ApiOperation("通过商品id获取商品详细信息")
    public CommonResult getGoodsById(String id) {
        Assert.notNull(id, "商品id不能为空！");
        return CommonResult.okResult(goodsService.getById(id));
    }

    @GetMapping("/{id}")
    @ApiOperation("通过商品id获取商品详细信息，RESTful风格")
    public CommonResult getGoodsByGoodsId(@PathVariable("id") String id) {
        Assert.notNull(id, "商品id不能为空！");
        return CommonResult.okResult(goodsService.getById(id));
    }


    @GetMapping("/user")
    @ApiParam("获取指定用户的全部商品")
    public CommonResult getAllGoodsListByUser(HttpServletRequest request) {
        String user = (String) request.getAttribute(SystemConstant.USER_ID);
        return goodsService.searchGoodsByUser(user);
    }

    /**
     * 分页+按照关键字模糊查询
     * 如果没有穿查询关键字，那么默认查询全部
     * 默认分页为第一页开始，一次查询10条数据
     *
     * @param key
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("分页搜索商品")
    public CommonResult getGoodsListByPage(@ApiParam("搜索关键字") String key,
                                           @ApiParam("第几页") Integer page,
                                           @ApiParam("页数大小") Integer size) {
        if (page < 0 || size <= 0) {
            return CommonResult.error("指定查询分页数据错误!");
        }
        page = page == 0 ? 1 : page;
        size = size == 0 ? 10 : size;
        return goodsService.getGoodsByPage(key, page, size);
    }

    /**
     * 通过分类查询数据
     *
     * @param categoryName
     * @return
     */
    @GetMapping("/searchByCategory")
    @ApiOperation("通过分类搜索商品")
    public CommonResult searchGoodsByCategory(String categoryName) {
        if (!StringUtils.hasText(categoryName)) {
            return CommonResult.error("请输入分类关键字！");
        }
        return goodsService.searchByCategory(categoryName);
    }

    /**
     * 通过关键字查询商品数据
     *
     * @param key 搜索关键字
     * @return
     */
    @GetMapping("/searchByName")
    @ApiOperation("通过名称搜索商品，同时搜索name、title、description")
    public CommonResult searchGoodsByName(@ApiParam("搜索关键字")
                                          @RequestParam("goodsName") String key) {
        return goodsService.searchGoodsByName(key);
    }

    @GetMapping("/searchByCategoryId")
    @ApiOperation("通过分类编号搜索商品")
    public CommonResult searchByCategoryId(@ApiParam("分类编号")
                                           @RequestParam("categoryId") String categoryId) {
        return goodsService.searchByCategoryId(categoryId);
    }


    @PostMapping
    @ApiOperation("添加商品")
    public CommonResult addGoods(HttpServletRequest request,
                                 @RequestBody Goods goods) {
        goods.setUserId((String) request.getAttribute("userId"));
        return goodsService.addGoods(request, goods);
    }


    /**
     * 上传商品图片
     *
     * @param request
     * @param file    图片文件
     * @return 图片链接
     */
    @PostMapping("/img")
    @ApiOperation("添加商品图片")
    public CommonResult uploadGoods(
            HttpServletRequest request,
            @RequestParam("goodsId") String goodsId,
            @ApiParam("商品图片文件")
            @RequestParam(value = "file", required = false) MultipartFile[] file) {
        return goodsService.uploadImg(goodsId, file);

    }

    @PostMapping("/withImg")
    @ApiOperation("添加商品，同时上传图片文件")
    public CommonResult addGoods(HttpServletRequest request,
                                 @RequestParam("file") MultipartFile[] files, Goods goods) {
        return goodsService.addGoods(request, files, goods);
    }

    @PutMapping
    public CommonResult updateGoods(
            HttpServletRequest request,
            @RequestBody Goods goods) {
        String userId = SysUtils.getUerId(request);
        goods.setUserId(userId);
        return goodsService.editGoods(goods);
    }

    @DeleteMapping
    public CommonResult deleteGoods(@Validated
                                    @NotNull
                                    @RequestBody Goods goods) {
        return goodsService.removeGoods(goods);
    }

    @DeleteMapping("/batch")
    public CommonResult deleteGoods(@RequestBody List<Goods> goods) {
        return goodsService.removeGoodsBatch(goods);
    }


    @PostMapping("/buy")
    public CommonResult buyGoods(HttpServletRequest request, @RequestBody Order order) {
        order.setBuyerName((String) request.getAttribute("userId"));
        return goodsService.buyGoods(order);
    }


    @GetMapping("/count")
    @ApiOperation("获取全部商品数量")
    public CommonResult getGoodsCount() {
        return CommonResult.okResult(goodsService.count());
    }

}
