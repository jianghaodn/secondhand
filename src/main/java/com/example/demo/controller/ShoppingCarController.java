package com.example.demo.controller;


import com.example.demo.pojo.DO.ShoppingCar;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.ShoppingCarService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/shoppingCar")
public class ShoppingCarController {
    private final ShoppingCarService shoppingCarService;

    public ShoppingCarController(ShoppingCarService shoppingCarService) {
        this.shoppingCarService = shoppingCarService;
    }

    @GetMapping
    @ApiOperation("获取某人的全部购物车信息")
    public CommonResult getCars(HttpServletRequest request) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        if (Objects.isNull(userId)) {
            return CommonResult.error("无权限操作");
        }
        return shoppingCarService.getAll(userId);
    }

    @GetMapping("/all")
    @ApiOperation("获取全部的购物车（管理员操作）")
    public CommonResult getCars() {
        return shoppingCarService.getAll(null);
    }

    @PostMapping
    @ApiOperation("添加商品到购物车")
    public CommonResult addCar(HttpServletRequest request,
                               @RequestBody ShoppingCar shoppingCar) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        shoppingCar.setUserName(userId);
        return shoppingCarService.addCar(shoppingCar);
    }

    @DeleteMapping
    @ApiOperation("删除购物车信息，需要商品id和购物车id")
    public CommonResult deleteCar(HttpServletRequest request, @RequestBody ShoppingCar shoppingCar) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        shoppingCar.setUserName(userId);
        return shoppingCarService.removeCar(shoppingCar);
    }

    @DeleteMapping("batch")
    @ApiOperation("批量删除购物车信息，需要商品id和购物车id")
    public CommonResult deleteCars(HttpServletRequest request,
                                   @ApiParam("数组")@RequestBody List<ShoppingCar> shoppingCars) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        for (ShoppingCar shoppingCar : shoppingCars) {
            shoppingCar.setUserName(userId);
        }
        return shoppingCarService.removeCarBatch(shoppingCars);
    }

    @PutMapping
    @ApiOperation("更新购物车信息")
    public CommonResult updateCar(HttpServletRequest request, @RequestBody ShoppingCar shoppingCar) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        shoppingCar.setUserName(userId);
        return shoppingCarService.editCar(shoppingCar);
    }


    /**
     * 获取指定商品是否已经加购
     *
     * @param request
     * @return 已经加购：true/没有加购：false
     */
    @PostMapping("/getStatus")
    @ApiOperation("返回商品是否加购，需要商品id")
    public CommonResult isAdd(HttpServletRequest request, @RequestBody ShoppingCar shoppingCar) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        shoppingCar.setUserName(userId);
        return CommonResult.okResult(shoppingCarService.getStatus(shoppingCar));
    }
}
