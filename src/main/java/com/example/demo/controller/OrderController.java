package com.example.demo.controller;

import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 获得某人的所有的订单(分页)
     *
     * @param request
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/getOrdersByPage")
    public CommonResult getOrdersByUserId(HttpServletRequest request,
                                          Integer page, Integer size) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        return orderService.selectByPage(userId, page, size);
    }

    /**
     * 获得某人的所有的订单
     * @param request
     * @return
     */
    @GetMapping("/getOrdersByUserId")
    public CommonResult getOrdersByUserId(HttpServletRequest request) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        return orderService.getByUserId(userId);
    }

    /**
     * 获取全部订单
     * @return
     */
    @GetMapping("/getOrders")
    public CommonResult getOrdersList() {
        return CommonResult.okResult(orderService.list());
    }

    /**
     * 增加一条订单
     *
     * @param order
     * @return
     */
    @PostMapping("/addOrder")
    public CommonResult createOrder(@RequestBody Order order) {
        return orderService.addOrder(order);
    }

    /**
     * 通过id获取一条订单信息
     *
     * @param orderId
     * @return
     */
    @GetMapping("/getOne")
    public CommonResult getOrderById(String orderId) {
        return CommonResult.okResult(orderService.getById(orderId));
    }

    /**
     * 更新订单信息
     *
     * @param order
     * @return
     */
    @PutMapping("/updateOrder")
    public CommonResult updateOrder(@RequestBody Order order) {
        return orderService.editOrder(order);
    }

    /**
     * 删除订单信息
     *
     * @param order
     * @return
     */
    @DeleteMapping
    public CommonResult deleteOrder(@RequestBody Order order) {
        return orderService.removeOrder(order);
    }
}
