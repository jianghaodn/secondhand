package com.example.demo.controller;


import cn.hutool.core.collection.ListUtil;
import com.example.demo.pojo.DO.Category;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author Administrator
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 获取全部分类
     *
     * @return
     */
    @GetMapping
    public CommonResult getAllCategories() {
        return CommonResult.okResult(categoryService.list());
    }

    @GetMapping("/admin")
    public CommonResult getAllCategoriesAdmin() {
        return categoryService.listAll();
    }

    @GetMapping("/{id}")
    public CommonResult getCategoryById(@PathVariable("id") String id) {
        return CommonResult.okResult(categoryService.getById(id));
    }

    @GetMapping("/admin/{id}")
    public CommonResult getCategoryByIdAdmin(@PathVariable("id") String id) {
        return categoryService.getByIdAdmin(id);
    }


    /**
     * 添加新的分类
     *
     * @param request
     * @param category
     * @return
     */
    @PostMapping
    public CommonResult addCategory(HttpServletRequest request, @RequestBody Category category) {

        return categoryService.addCategory(category);
    }


    /**
     * 删除分类，可以同时删除多个分类，或者只删除一个分类
     *
     * @param ids 分类id
     * @return
     */
    @DeleteMapping
    public CommonResult deleteCategory(HttpServletRequest request,
                                       @RequestBody List<String> ids) {
        System.out.println(request.getAttribute(SystemConstant.USER_ID));
        return categoryService.removeCategory(request, ids);
    }

    @DeleteMapping("/{id}")
    public CommonResult deleteCategory(HttpServletRequest request,
                                       @PathVariable("id") String id) {
        return categoryService.removeCategory(request, ListUtil.of(id));
    }


    /**
     * 修改分类信息
     *
     * @param category 新的分类信息
     * @return
     */
    @PutMapping
    public CommonResult updateCategory(@RequestBody Category category) {
        return categoryService.editCategory(category);
    }


    /**
     * 导出全部分类
     *
     * @param request
     */
    @GetMapping("/export")
    public void exportData(HttpServletRequest request) {
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        categoryService.export(userId);
    }
}
