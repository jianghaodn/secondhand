package com.example.demo.utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Bean复制工具类
 */
public class BeanCopyUtils {
    private BeanCopyUtils(){

    }

    /**
     * 单个bean的copy方法
     * @param source
     * @param clazz target的字节码对象
     * @return
     * @param <T>
     */
    public static <T> T copy(Object source,Class<T> clazz){
        T t;
        try {
            t = clazz.newInstance();
            BeanUtils.copyProperties(source,t);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return t;
    }

    /**
     * 复制集合的方法
     * @param source 源集合
     * @param clazz 目标对象的字节码对象
     * @return
     * @param <T>
     * @param <R>
     */
    public static <T,R> List<R> copyList(List<T> source,Class<R> clazz){
        return source.stream()
                .map(o->copy(o,clazz))
                .collect(Collectors.toList());
    }
}
