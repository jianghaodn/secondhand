package com.example.demo.utils;

import com.example.demo.mapper.UserMapper;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * 系统工具类
 */
@Component
public class SysUtils {

    @Resource
    private UserService userService;
    public static String getUerId(HttpServletRequest request){
        return (String) request.getAttribute(SystemConstant.USER_ID);
    }

    /**
     * 验证用户是否是管理员
     * @param userId
     * @return
     */
    public boolean checkUserIsAdmin(String userId){
        System.out.println("用户为："+userId);
        User byId = userService.getById(userId);
        return Objects.nonNull(byId) && byId.getAdmin();
    }
}
