package com.example.demo.utils;

import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 向前端响应信息的工具类，用于servlet发生异常的情况
 * @author Administrator
 */
public class WebUtils
{
    public static<T> void render(HttpServletResponse response,T t){
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        try {
            PrintWriter writer = response.getWriter();
            writer.write(JSON.toJSONString(t));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
