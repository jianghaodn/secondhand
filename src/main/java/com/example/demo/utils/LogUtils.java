package com.example.demo.utils;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 打印访问者的一些信息
 */
@Component
@Slf4j
public class LogUtils {

    /**
     * 打印访问者的ip、端口等信息
     * @param request
     */
    public void log(HttpServletRequest request) {
        log.info("-------打印访问者信息-------");
        log.info("远程用户名："+request.getRemoteUser());
        log.info("远程用户地址："+request.getRemoteAddr());
        log.info("远程用户Host："+request.getRemoteHost());
        log.info(String.valueOf("远程用户端口："+request.getRemotePort()));
    }

    /**
     * 打印服务器的信息
     * @param request
     */
    public void logLocal(HttpServletRequest request){
        log.info("本地用户名："+request.getLocalName());
        log.info("本地用户地址："+request.getLocalAddr());
        log.info("本地端口："+request.getLocalPort());
    }

}
