package com.example.demo.utils;

import cn.hutool.core.util.ArrayUtil;
import com.example.demo.pojo.common.SystemConstant;

import java.util.Objects;

/**
 * @author Administrator
 */
public class StrUtils {

    /**
     * 检查字符串是否合法
     * 用于检查用户名、密码等的合法性，包括长度是否合法、是否包含非法字符等
     * @param str 验证的字符串
     * @param min 最小长度，可为空
     * @param max 最大长度，可为空
     * @return 验证通过为true，否则为false
     */
    public static Boolean checkString(String str,Integer min,Integer max){
        if (Objects.nonNull(min) && Objects.nonNull(max)){
            if (str.length()<min|| str.length()>max){
                return false;
            }
        }
        return !str.matches(SystemConstant.CHECK_STR_REG);
    }

    /**
     * 将字符串转为byte数组
     * 例如。str=999，则bytes=[9,9,9]
     * @param str
     * @return
     */
    public static byte[] strToBytes(String str){
        String[] split = str.split(SystemConstant.ENCRYPT_SPLIT);
        System.out.println(ArrayUtil.join(split,""));
        byte []b = new byte[split.length];
        for (int i = 0; i < split.length; i++) {
            b[i] = Byte.parseByte(split[i]);
        }
        return b;
    }
}
