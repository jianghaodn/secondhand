package com.example.demo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.ShoppingCarMapper;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.DO.ShoppingCar;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.service.GoodsService;
import com.example.demo.service.ShoppingCarService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Administrator
 */
@Service
public class ShoppingCarServiceImpl extends ServiceImpl<ShoppingCarMapper, ShoppingCar>
        implements ShoppingCarService {

    @Resource
    private GoodsService goodsService;

    /**
     * 将商品加入到购物车
     * @param shoppingCar 购物车信息
     * @return 加入结果
     */
    @Override
    public CommonResult addCar(ShoppingCar shoppingCar) {
        String goodsId = shoppingCar.getGoodsId();
        //判断商品是否还有剩余
        Goods goods = goodsService.getOne(
                new LambdaQueryWrapper<Goods>().eq(Goods::getId, goodsId));
        if (Objects.isNull(goods)){
            return CommonResult.error("商品库存不足");
        }
        //判断是否已经加入购物车
        if (getStatus(shoppingCar)){
            return CommonResult.error("您已经添加过了！");
        }
        getBaseMapper().insert(shoppingCar);
        return CommonResult.okResult();
    }


    /**
     * 将商品从购物车移除
     * @param shoppingCar 购物车信息
     * @return 移除结果
     */
    @Override
    public CommonResult removeCar(ShoppingCar shoppingCar) {
        //先检查这个购物车存不存在
        if (getStatus(shoppingCar)){
            getBaseMapper().deleteById(shoppingCar.getId());
        }else {
            return CommonResult.error("不存在这条信息！");
        }
        return CommonResult.okResult();
    }

    @Override
    public CommonResult editCar(ShoppingCar shoppingCar) {
        if (getStatus(shoppingCar)){
            getBaseMapper().updateById(shoppingCar);
        }else {
            return CommonResult.error("不存在这条信息！");
        }
        return null;
    }

    @Override
    public CommonResult getAll(String userId) {
        LambdaQueryWrapper<ShoppingCar> queryWrapper = new LambdaQueryWrapper<ShoppingCar>()
                .eq(StringUtils.hasText(userId), ShoppingCar::getUserName, userId);
        List<ShoppingCar> shoppingCars = getBaseMapper().selectList(queryWrapper);
        return CommonResult.okResult(shoppingCars);
    }

    @Override
    public Boolean getStatus(ShoppingCar shoppingCar) {
        ShoppingCar shoppingCar1 = getBaseMapper().selectOne(
                new LambdaQueryWrapper<ShoppingCar>().eq(ShoppingCar::getGoodsId, shoppingCar.getGoodsId())
                        .eq(ShoppingCar::getUserName, shoppingCar.getUserName()));
        return Objects.nonNull(shoppingCar1);
    }

    @Override
    public CommonResult removeCarBatch(List<ShoppingCar> shoppingCars) {
        for (ShoppingCar shoppingCar : shoppingCars) {
            if (!getStatus(shoppingCar)){
                return CommonResult.error("购物车状态异常，删除失败");
            }
        }
        getBaseMapper().deleteBatchIds(shoppingCars.stream().map(ShoppingCar::getId)
                .collect(Collectors.toList()));
        return CommonResult.okResult();
    }
}
