package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.pojo.DO.UserEncrypt;
import com.example.demo.service.UserEncryptService;
import com.example.demo.mapper.UserEncryptMapper;
import org.springframework.stereotype.Service;

/**
* @author dahaozi
* @description 针对表【user_encrypt(保存用户密钥)】的数据库操作Service实现
* @createDate 2022-12-03 20:27:08
*/
@Service
public class UserEncryptServiceImpl extends ServiceImpl<UserEncryptMapper, UserEncrypt>
    implements UserEncryptService{

}




