package com.example.demo.service.impl;

import com.example.demo.mapper.GoodsAdminMapper;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.service.GoodsAdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class GoodsAdminServiceImpl implements GoodsAdminService {
    @Resource
    private GoodsAdminMapper goodsAdminMapper;

    @Override
    public CommonResult getGoodsList() {
        return CommonResult.okResult(goodsAdminMapper.selectAllGoods());
    }

    @Override
    public CommonResult getGoodsList(Integer page, Integer size) {
        int begin = 0;
        Integer count = goodsAdminMapper.selectCount();
        if (Objects.nonNull(page) && Objects.nonNull(size)) {
            begin = page * size;
            if (begin >= count) {
                return CommonResult.error("没有更多数据");
            }
        }
        List<Goods> goods = goodsAdminMapper.
                selectByPage(begin, Objects.isNull(size)|| size.equals(0)?count:size);
        return CommonResult.okResult(goods);
    }

    @Override
    public CommonResult getCount() {
        return CommonResult.okResult(goodsAdminMapper.selectCount());
    }

    @Override
    public CommonResult getGoodsInfoById(String id) {
        return CommonResult.okResult(goodsAdminMapper.selectGoodsById(id));
    }

    @Override
    public CommonResult removeGoodsById(Goods goods) {

        return  CommonResult.okResult();
    }

    @Override
    public CommonResult modifyGoods(Goods goods) {
        int result = goodsAdminMapper.updateById(goods);
        return CommonResult.okResult();
    }
}
