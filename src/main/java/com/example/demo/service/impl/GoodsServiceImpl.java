package com.example.demo.service.impl;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.common.SystemException;
import com.example.demo.pojo.DO.Category;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.service.CategoryService;
import com.example.demo.service.GoodsService;
import com.example.demo.mapper.GoodsMapper;
import com.example.demo.service.OrderService;
import com.example.demo.service.UserService;
import com.example.demo.utils.FileUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Administrator
 */
@Service
@Slf4j
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods>
        implements GoodsService {
    @Resource
    private FileUtils fileUtils;
    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private OrderService orderService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private UserService userService;

    @Override
    public CommonResult getGoodsByPage(String key, Integer page, Integer size) {
        Page<Goods> objectPage = new Page<>(page, size);
        goodsMapper.selectPage(objectPage, new LambdaQueryWrapper<Goods>()
                .like(StringUtils.hasText(key), Goods::getName, key));
        return CommonResult.okResult(MapUtil.builder().put("total", objectPage.getTotal())
                .put("data", objectPage.getRecords()).map());
    }

    /**
     * 发布商品
     *
     * @param goods 商品信息
     * @return 发布结果
     */
    @Override
    public CommonResult addGoods(HttpServletRequest request, Goods goods) {
        if (!StringUtils.hasText(goods.getTitle()) || Objects.isNull(goods.getPrice())) {
            return CommonResult.error("请输入正确的商品信息！");
        }
        String userId = (String) request.getAttribute(SystemConstant.USER_ID);
        goods.setUserId(userId);

        String categoryId = goods.getCategoryId();
        Category category = categoryService.getById(categoryId);
        if (Objects.isNull(category)) {
            return CommonResult.error("商品分类不存在");
        }
        goodsMapper.insert(goods);
        return CommonResult.okResult(MapUtil.builder().put("goodsId", goods.getId()).map());
    }

    @Override
    public CommonResult editGoods(Goods goods) {
        //先查询这个商品是否存在
        User user = userService.getById(goods.getUserId());
        System.out.println(user);
        Goods goods1;
        if (user.getAdmin()) {
            System.out.println("管理员操作");
            goods1 = goodsMapper.selectOne(new LambdaQueryWrapper<Goods>().eq(Goods::getId, goods.getId())
                    .eq(Goods::getIsDeleted, goods.getIsDeleted())
            )
//                    .or()
//                    .eq(Goods::getIsDeleted,false))
            ;
        } else {
            System.out.println("普通用户操作");
            goods1 = goodsMapper.selectById(goods.getId());
        }
        if (goods1 == null) {
            return CommonResult.error("修改失败，商品不存在");
        }
        goodsMapper.updateById(goods);
        return CommonResult.okResult();
    }

    @Override
    public CommonResult removeGoods(Goods goods) {
        return CommonResult.okResult(goodsMapper.deleteById(goods.getId()));
    }

    /**
     * 购买商品
     * 因为servlet是多线程的，因此必须使用synchronized进行处理，防止购买数据出错
     *
     * @param order 订单信息
     * @return 购买结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public CommonResult buyGoods(Order order) {
        synchronized (GoodsServiceImpl.class) {
            //查询该商品是否存在
            String goodsId = order.getGoodsId();
            Goods goods1 = goodsMapper.selectById(goodsId);
            if (Objects.isNull(goods1)) {
                return CommonResult.error("商品不存在！");
            }
            if (goods1.getNum() == 0) {
                return CommonResult.error("已经售罄!");
            }
            if (order.getNum() < goods1.getNum()) {
                return CommonResult.error("库存不足！");
            }
            //更新商品表
            goods1.setNum(goods1.getNum() - order.getNum());
            goodsMapper.updateById(goods1);
            //生成订单信息
            orderService.addOrder(order);
            return CommonResult.result("购买成功");
        }
    }

    @Override
    public CommonResult searchByCategory(String categoryName) {
        List<Goods> goods = goodsMapper.selectGoodsByCategory(categoryName);
        System.out.println(goods);
        return CommonResult.okResult(goods);
    }

    @Override
    public CommonResult searchGoodsByName(String key) {
        //关键词太长，返回错误信息
        if (key.length() > SystemConstant.GOODS_NAME_MAX_LENGTH) {
            return CommonResult.error("关键词太长了，换个短点的吧");
        }
        LambdaQueryWrapper<Goods> queryWrapper;

        if (StringUtils.hasText(key)) {
            queryWrapper = new LambdaQueryWrapper<Goods>().like(Goods::getName, key).or().
                    like(Goods::getDescription, key).or().like(Goods::getTitle, key);
            List<Goods> goods = goodsMapper.selectList(queryWrapper);
            return CommonResult.okResult(goods);
        }
        return CommonResult.okResult();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = SystemException.class)
    public CommonResult uploadImg(String goodsId, MultipartFile[] files) {

        if (files == null || files.length == 0) {
            return CommonResult.okResult();
        }
        List<String> urls = new ArrayList<>();
        for (MultipartFile multipartFile : files) {
            try {
                String url = fileUtils.fileUpload(multipartFile.getInputStream());
                urls.add(url);
            } catch (IOException e) {
                return CommonResult.error("上传图片失败");
            }
        }
        //将图片链接保存起来
        for (String url : urls) {
            goodsMapper.insertImg(goodsId, url);
        }
        return CommonResult.okResult(MapUtil.builder().put("files", urls).map());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public CommonResult addGoods(HttpServletRequest request, MultipartFile[] files, Goods goods) {
        //先将 物品存入数据库，然后物品就有id，再将 图片存入数据库
        CommonResult commonResult1 = this.addGoods(request, goods);


        //将 图片存入数据库
        CommonResult commonResult =
                this.uploadImg((String) ((HashMap) commonResult1.getData()).get("goodsId"), files);

        return commonResult;
    }

    @Override
    public CommonResult searchGoodsByUser(String user) {
        List<Goods> goods = getBaseMapper().selectList(
                new LambdaQueryWrapper<Goods>().eq(Goods::getUserId, user));
        return CommonResult.okResult(goods);
    }

    @Override
    public CommonResult searchByCategoryId(String categoryId) {
        List<Goods> goodsList = goodsMapper.selectGoodsByCategoryId(categoryId);
        return CommonResult.okResult(goodsList);
    }

    @Override
    public CommonResult removeGoodsBatch(List<Goods> goods) {
        getBaseMapper().deleteBatchIds(goods.stream()
                .map(Goods::getId).collect(Collectors.toList()));
        return CommonResult.okResult();
    }
}




