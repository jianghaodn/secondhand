package com.example.demo.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.PageVo;
import com.example.demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Administrator
 */
@Service
@Slf4j
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order>
        implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public CommonResult removeOrder(Order o) {
        orderMapper.deleteById(o);
        return CommonResult.okResult();
    }

    @Override
    public CommonResult selectByPage(String user, int page, int limit) {
        Page<Order> objectPage = new Page<>(page, limit);
        LambdaQueryWrapper<Order> queryWrapper = new LambdaQueryWrapper<Order>().eq(Order::getBuyerName, user);
        orderMapper.selectPage(objectPage,queryWrapper);
        return CommonResult.okResult(new PageVo<>(objectPage.getTotal(),objectPage.getRecords()));
    }

    @Override
    public CommonResult editOrder(Order order) {
        orderMapper.updateById(order);
        return CommonResult.okResult();
    }

    /**
     * 生成订单数据
     * @param order 订单
     * @return 生成结果
     */
    @Override
    public CommonResult addOrder(Order order) {
        order.setId(IdUtil.simpleUUID());
        orderMapper.insert(order);
        return CommonResult.okResult();
    }


    /**
     * 获取用户全部的订单数据
     * @param userId 用户 id，如果为空，说明是获取所有用户的订单数据
     * @return 订单数据
     */
    @Override
    @Transactional(readOnly = true)
    public CommonResult getByUserId(String userId) {
        List<Order> orders = orderMapper.
                selectList(new LambdaQueryWrapper<Order>().eq(StringUtils.hasText(userId),
                        Order::getBuyerName, userId));
        return CommonResult.okResult(orders);
    }
}
