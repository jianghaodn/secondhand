package com.example.demo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.CategoryMapper;
import com.example.demo.mapper.GoodsMapper;
import com.example.demo.pojo.DO.Category;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.service.CategoryService;
import com.example.demo.service.UserService;
import com.example.demo.utils.SysUtils;
import com.example.demo.utils.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author Administrator
 */
@Service
@Slf4j
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category>
        implements CategoryService {
    @Resource
    private UserService userService;

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private GoodsMapper goodsMapper;
    @Resource
    private SysUtils sysUtils;

    @Override
    public CommonResult addCategory(Category category) {
        if (getStatus(category)) {
            return CommonResult.error("该分类已经存在！");
        }
        getBaseMapper().insert(category);
        return CommonResult.okResult();
    }

    @Override
    @Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRED)
    public CommonResult removeCategory(HttpServletRequest request, List<String> ids) {
        //验证是否是管理员
        String userId = SysUtils.getUerId(request);
        if (!sysUtils.checkUserIsAdmin(userId)) {
            return CommonResult.result(HttpStatus.FORBIDDEN.value(),"无权限操作");
        }
        for (String id : ids) {
            //先搜索该分类下面是否包含商品，如果有，则不能删除
            List<Goods> goods = goodsMapper.selectGoodsByCategoryId(id);
            if (Objects.nonNull(goods) && goods.size() != (0)) {
                //抛出一个异常，让事务回滚
                throw new RuntimeException("分类下包含在售商品，不能删除!");

            }
            getBaseMapper().deleteById(id);
        }
        getBaseMapper().deleteBatchIds(ids);
        return CommonResult.okResult();
    }

    @Override
    public CommonResult editCategory(Category category) {
//        if (getStatus(category)) {
            getBaseMapper().updateById(category);
//        } else {
//            return CommonResult.error("分类不存在！");
//        }
        return CommonResult.okResult();
    }

    @Override
    public void export(String userId) {

    }

    @Override
    public CommonResult getByIdAdmin(String id) {
        return CommonResult.okResult(categoryMapper.selectByIdAdmin(id));
    }

    @Override
    public CommonResult listAll() {

        return CommonResult.okResult(categoryMapper.selectAll());
    }


    /**
     * 查询一个分类信息是否存在
     *
     * @param category
     * @return
     */
    public Boolean getStatus(Category category) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<Category>().
                eq(Category::getName, category.getName());
        Category one = getBaseMapper().selectOne(queryWrapper);
        return Objects.nonNull(one);
    }

    public Boolean getStatus(String id) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<Category>().
                eq(Category::getName, id);
        Category category = getBaseMapper().selectOne(queryWrapper);
        return Objects.nonNull(category);
    }
}
