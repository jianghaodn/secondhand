package com.example.demo.service.impl;

import cn.hutool.core.map.MapUtil;
import com.alibaba.excel.EasyExcel;
import com.example.demo.mapper.UserAdminMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.pojo.DO.Category;
import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.service.CategoryService;
import com.example.demo.service.GoodsAdminService;
import com.example.demo.service.UserAdminService;
import com.example.demo.service.UserService;
import com.example.demo.utils.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

@Service
public class UserAdminServiceImpl implements UserAdminService {

    @Resource
    private UserService userService;

    @Resource
    public UserMapper userMapper;
    @Resource
    private UserAdminMapper userAdminMapper;

    @Resource
    private GoodsAdminService goodsAdminService;
    @Resource
    private FileUtils fileUtil;

    @Resource
    private CategoryService categoryService;


    /**
     * 审核用户信息
     * @param user 用户信息
     * @return 审核结果
     */
    @Override
    public CommonResult settingUser(User user) {
        userService.updateById(user);
        return CommonResult.okResult();
    }

    @Override
    public CommonResult settingGoods() {
        return null;
    }

    @Override
    public CommonResult getAllCount() {
        return null;
    }


    /**
     * 导出全部用户数据为excel文件
     * @param response
     */
    @Override
    @Transactional(readOnly = true)
    public void exportUserData(HttpServletResponse response) {
        //先获取全部的用户数据
        List<User> userList = userService.list();
        fileUtil.exportData(response,userList,User.class,"用户数据");
    }

    @Override
    public void exportGoodsData(HttpServletResponse response) {
        CommonResult goodsList = goodsAdminService.getGoodsList();
        List<Goods> goodsList1 = (List<Goods>)goodsList.getData();
        System.out.println(goodsList1);
        fileUtil.exportData(response,goodsList1,Goods.class,"商品");
    }

    @Override
    public CommonResult exportOrdersData() {
        return null;
    }

    @Override
    public CommonResult viewCount() {
        return null;
    }

    @Override
    public CommonResult setUserStatus(User user) {
        userAdminMapper.updateUserStatus(user);
        return CommonResult.success();
    }

    @Override
    public CommonResult getUsersByPage(Integer page, Integer size) {
        int count = userAdminMapper.selectCount();
        int begin = page * size;
        size = count- begin >=size?size:count- begin;
        List<User> users = userAdminMapper.selectByPage(begin,size );
        return CommonResult.okResult(MapUtil.builder().put("total",count)
                .put("users",users).map());
    }

    @Override
    public CommonResult getUserByName(String name) {

        List<User> users = userAdminMapper.selectUserByName(name);
        return CommonResult.okResult(users);
    }

    @Override
    public void exportCategoryData(HttpServletResponse response) {
        List<Category> list = categoryService.list();
        fileUtil.exportData(response,list, Category.class,"分类数据");
    }


}
