package com.example.demo.service.impl;


import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.UserAddressMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.DO.UserAddress;
import com.example.demo.pojo.DO.UserEncrypt;
import com.example.demo.pojo.common.CommonResult;
import com.example.demo.pojo.common.SystemConstant;
import com.example.demo.pojo.vo.UserVo;
import com.example.demo.service.UserEncryptService;
import com.example.demo.service.UserService;
import com.example.demo.utils.BeanCopyUtils;
import com.example.demo.utils.JwtUtil;
import com.example.demo.utils.StrUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Administrator
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    @Resource
    private UserEncryptService userEncryptService;

    private static final String[] prefixName = {
            "善良", "性格", "品质", "聪明", "坏蛋", "性格"
            , "知识 ", "愚蠢", "诚信", "慈祥", "胆小", "勇敢"
    };
    private static final String[] suffixName = {
            "春天", "夏天", "秋天", "冬天", "六月",
            "热", "冷", "太阳", "星星", "月亮", "山",
            "花", "水 ", "声音", "阳光", "风", "大雪",
            "早晨", "中午", "下午", "晚上", "闪电", "雪",
            "沙漠", "地震", "洪水", "大雨", "雨"
    };

    private static final String PATTERN = "ˇ$";


    @Resource
    private UserMapper userMapper;

    @Resource
    private UserAddressMapper userAddressMapper;

    @Override
    public CommonResult editUser(User u) {
        if (!StringUtils.hasText(u.getId())) {
            return CommonResult.error();
        }
        if (StringUtils.hasText(u.getPassword())&&!StrUtils.checkString(u.getPassword(), SystemConstant.PASSWORD_MIN_LENGTH, SystemConstant.PASSWORD_MAX_LENGTH)) {
            return CommonResult.error("密码不合法，请检查");
        }
        //更新密码，先拿到用户的密钥，再重新加密以后更新密码
        User user = userMapper.selectById(u.getId());
        String password = user.getPassword();
        if (StringUtils.hasText(u.getPassword()) && !password.equals(u.getPassword())) {
            //需要更新密码
            String encrypt = getEncrypt(u);
            if (StringUtils.hasText(encrypt)) {
                String newPassword = encrypt(encrypt, u.getPassword());
                u.setPassword(newPassword);
            } else {
                return CommonResult.error("用户信息异常");
            }
        }
        userMapper.updateById(u);
        return CommonResult.okResult();
    }


    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public CommonResult login(User user) {
        //判断用户名是否合法
        if (!StringUtils.hasText(user.getId()) && !StringUtils.hasText(user.getUsername())) {
            return CommonResult.error("用户id错误");
        }
        //判断用户密码是否合法
        if (!StringUtils.hasText(user.getPassword())) {
            return CommonResult.error("用户密码错误");
        }
        //从数据库中查询这个用户信息
        User u;
        String id;
        if (user.getUsername() != null) {
            id = user.getUsername();
        } else {
            id = user.getId();
        }
        u = userMapper.selectById(id);
        //获取密钥
        String encrypt = getEncrypt(user);
        if (Objects.isNull(encrypt) || Objects.isNull(u) || !u.getPassword().equals(encrypt(encrypt, user.getPassword().trim()))) {
            return CommonResult.error("用户名或者密码错误，登录失败");
        }

        //将本次登录时间更新至数据库
        Date date = new Date();
        u.setLastLogin(date);
        getBaseMapper().updateById(u);
        Object returnUser;
        if (!u.getAdmin()){
            returnUser = BeanCopyUtils.copy(u, UserVo.class);
        }else {
            returnUser = u;
        }
        //登录成功，以用户id生成token返回给前端
        String jwt = JwtUtil.createJWT(id);

        Map<Object, Object> map = MapUtil.builder().put("user", returnUser).put("token", jwt).map();
        return CommonResult.result(200, "登录成功", map);
    }

    /**
     * 获取一个用户的密钥
     *
     * @param user
     * @return
     */
    private String getEncrypt(User user) {
        UserEncrypt one = userEncryptService.getOne(new LambdaQueryWrapper<UserEncrypt>()
                .eq(UserEncrypt::getUsername, user.getId() == null ? user.getUsername() : user.getId()));
        return Objects.isNull(one) ? null : one.getEncrypt();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public CommonResult register(User user) {
        //验证用户名是否合法
        if (!StringUtils.hasText(user.getId())) {
            return CommonResult.error("请输入用户名！");
        }
        //验证用户名是否已经存在
        User user1 = userMapper.selectById(user.getId());
        if (Objects.nonNull(user1)) {
            return CommonResult.error("用户名已经存在!");
        }
        if (!StringUtils.hasText(user.getPassword())
                || !StrUtils.checkString(user.getPassword(), SystemConstant.PASSWORD_MIN_LENGTH, SystemConstant.PASSWORD_MAX_LENGTH)) {
            return CommonResult.error("密码不合法！");
        }

        //加密用户密码以后再存储
        String newPassword = encrypt(user);
        user.setPassword(newPassword);

        //如果用户没有设置昵称，生成默认的昵称
        if (!StringUtils.hasText(user.getNickname())) {
            user.setNickname(generateNickName());
        } else if (user.getNickname().length() > SystemConstant.NICKNAME_MAX_LENGTH) {
            return CommonResult.error("昵称过长");
        }
        //插入数据库，返回注册结果
        user.setStatus(false);
        int insert = userMapper.insert(user);
        return CommonResult.okResult(insert == 1 ? "注册成功" : "注册失败");
    }

    /**
     * 生成一个随机的昵称
     *
     * @return
     */
    private String generateNickName() {
        return prefixName[RandomUtil.randomInt(0, prefixName.length)]
                + "的" +
                suffixName[RandomUtil.randomInt(0, suffixName.length)];
    }

    @Override
    public CommonResult getUserInfo(String user) {
        HashMap<Object, Object> hashMap = new HashMap<>();
        hashMap.put("user", userMapper.selectById(user));
        return CommonResult.okResult(hashMap);
    }

    @Override
    public CommonResult editPassword(User user) {
        if (!StringUtils.hasText(user.getPassword())) {
            return CommonResult.error();
        }
        //TODO 验证用户信息以后才能修改密码
        String s = SecureUtil.md5(user.getPassword());
        user.setPassword(s);
        userMapper.updateById(user);
        return CommonResult.okResult();
    }

    /**
     * 新增用户地址
     *
     * @param userAddress 用户地址对象
     * @return 操作结果
     */
    @Override
    public CommonResult addAddress(UserAddress userAddress) {
        String addressName = userAddress.getAddressName();
        if (!StringUtils.hasText(addressName)) {
            return CommonResult.error("请填写地址！");
        }
        //验证地址是否已经存在
        LambdaQueryWrapper<UserAddress> queryWrapper = new LambdaQueryWrapper<UserAddress>()
                .eq(UserAddress::getUserId, userAddress.getUserId())
                .eq(UserAddress::getAddressName, addressName);
        UserAddress userAddress1 = userAddressMapper.selectOne(queryWrapper);
        if (Objects.nonNull(userAddress1)) {
            return CommonResult.error("地址已经存在");
        }
        userAddressMapper.insert(userAddress);
        return CommonResult.okResult();
    }

    @Override
    public CommonResult removeAddress(UserAddress userAddress) {
        userAddressMapper.delete(new LambdaQueryWrapper<UserAddress>().eq(UserAddress::getUserId, userAddress.getUserId())
                .eq(UserAddress::getAddressName, userAddress.getAddressName()));
        return CommonResult.okResult();
    }

    @Override
    public CommonResult getUsersByPage(Integer page, Integer size) {
        Page<User> userPage = new Page<>(page, size);
        page(userPage);

        return CommonResult.okResult(MapUtil.builder().put("size", userPage.getTotal())
                .put("data", userPage.getRecords()).map());

    }

    @Override
    public CommonResult getUserByName(String name) {
        System.out.println("name = "+name);
        LambdaQueryWrapper<User> queryWrapper =
                new LambdaQueryWrapper<User>().eq(StringUtils.hasText(name), User::getNickname, name);
        User one = getBaseMapper().selectOne(queryWrapper);
        return CommonResult.okResult(one);
    }


    /**
     * 加密用户密码
     *
     * @param key 密钥
     * @param str 原密码
     * @return 加密后的密码
     */
    public static String encrypt(String key, String str) {
        //随机生成密钥
        byte[] bytes = StrUtils.strToBytes(key);
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, bytes);
        byte[] encrypt = aes.encrypt(StrUtil.bytes(str));
        return ArrayUtil.join(encrypt, "");
    }

    /**
     * 返回密码和密钥
     *
     * @param str 原字符串
     * @return 0：密钥；1：密码
     */
    public static List<String> encryptWithKey(String str) {
        //随机生成密钥
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        //构建
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, key);
        byte[] encrypt = aes.encrypt(StrUtil.bytes(str));
        String s = ArrayUtil.join(key, SystemConstant.ENCRYPT_SPLIT);
        return ListUtil.of(s, ArrayUtil.join(encrypt, ""));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String encrypt(User user) {
        String password = user.getPassword().trim();
        List<String> list = encryptWithKey(password);
        //将密钥存入数据库
        userEncryptService.save(new UserEncrypt(user.getId(), list.get(0)));
        return list.get(1);
    }
}

