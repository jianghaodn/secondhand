package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mapper.UserAddressMapper;
import com.example.demo.pojo.DO.UserAddress;
import com.example.demo.service.UserAddressService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserAddressServiceImpl extends ServiceImpl<UserAddressMapper, UserAddress>
        implements UserAddressService {

}
