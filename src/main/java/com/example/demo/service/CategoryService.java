package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.DO.Category;
import com.example.demo.pojo.common.CommonResult;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CategoryService extends IService<Category> {

    CommonResult addCategory(Category category);

    CommonResult removeCategory(HttpServletRequest request, List<String> ids);

    CommonResult editCategory(Category category);

    void export(String userId);

    CommonResult getByIdAdmin(String id);

    CommonResult listAll();
}
