package com.example.demo.service;

import com.example.demo.pojo.DO.Goods;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.common.CommonResult;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface GoodsService extends IService<Goods> {

    CommonResult getGoodsByPage(String key, Integer page, Integer size);

    CommonResult addGoods(HttpServletRequest request,Goods goods);

    CommonResult editGoods(Goods goods);

    CommonResult removeGoods(Goods goods);

    CommonResult buyGoods(Order goods);

    CommonResult searchByCategory(String categoryName);

    CommonResult searchGoodsByName(String goodsName);

    CommonResult uploadImg(String goodsId, MultipartFile[] file);

    CommonResult addGoods(HttpServletRequest request,MultipartFile[] files, Goods goods);

    CommonResult searchGoodsByUser(String user);

    CommonResult searchByCategoryId(String categoryId);

    CommonResult removeGoodsBatch(List<Goods> goods);
}
