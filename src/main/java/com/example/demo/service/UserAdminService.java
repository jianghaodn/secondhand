package com.example.demo.service;

import cn.hutool.http.server.HttpServerResponse;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.common.CommonResult;

import javax.servlet.http.HttpServletResponse;

public interface UserAdminService  {
    public CommonResult settingUser(User user) ;
    public CommonResult settingGoods() ;
    public CommonResult getAllCount() ;
    public void exportUserData(HttpServletResponse response);
    void exportGoodsData(HttpServletResponse response);

    public CommonResult exportOrdersData() ;
    public CommonResult viewCount();

    CommonResult setUserStatus(User user);

    CommonResult getUsersByPage(Integer page, Integer size);

    CommonResult getUserByName(String name);

    void exportCategoryData(HttpServletResponse response);
}
