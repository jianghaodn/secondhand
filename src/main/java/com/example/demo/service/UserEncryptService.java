package com.example.demo.service;

import com.example.demo.pojo.DO.UserEncrypt;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author dahaozi
* @description 针对表【user_encrypt(保存用户密钥)】的数据库操作Service
* @createDate 2022-12-03 20:27:08
*/
public interface UserEncryptService extends IService<UserEncrypt> {

}
