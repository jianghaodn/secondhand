package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.DO.User;
import com.example.demo.pojo.DO.UserAddress;
import com.example.demo.pojo.common.CommonResult;

/**
 *
 */
public interface UserService extends IService<User> {

    CommonResult editUser(User u);


    CommonResult login(User user);

    CommonResult register(User user);

    CommonResult getUserInfo(String user);

    CommonResult editPassword(User user);

    CommonResult addAddress(UserAddress userAddress);

    CommonResult removeAddress(UserAddress userAddress);

    CommonResult getUsersByPage(Integer page, Integer size);

    CommonResult getUserByName(String name);
}
