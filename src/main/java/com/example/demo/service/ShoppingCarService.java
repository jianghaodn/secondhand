package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.controller.ShoppingCarController;
import com.example.demo.pojo.DO.ShoppingCar;
import com.example.demo.pojo.common.CommonResult;

import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ShoppingCarService extends IService<ShoppingCar> {


    CommonResult addCar(ShoppingCar shoppingCar);

    CommonResult removeCar(ShoppingCar shoppingCar);

    CommonResult editCar(ShoppingCar shoppingCar);

    CommonResult getAll(String userId);

    Boolean getStatus(ShoppingCar shoppingCar);

    CommonResult removeCarBatch(List<ShoppingCar> shoppingCars);
}
