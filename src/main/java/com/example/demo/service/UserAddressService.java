package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.DO.UserAddress;

/**
 *
 */
public interface UserAddressService extends IService<UserAddress> {

}
