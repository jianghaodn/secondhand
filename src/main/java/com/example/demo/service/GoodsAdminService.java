package com.example.demo.service;

import com.example.demo.pojo.DO.Goods;
import com.example.demo.pojo.common.CommonResult;

public interface GoodsAdminService {

    CommonResult getGoodsList();

    CommonResult getGoodsList(Integer page, Integer size);

    CommonResult getCount();

    CommonResult getGoodsInfoById(String id);

    CommonResult removeGoodsById(Goods goods);

    CommonResult modifyGoods(Goods goods);
}
