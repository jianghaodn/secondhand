package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.pojo.DO.Order;
import com.example.demo.pojo.common.CommonResult;

/**
 *
 * @author Administrator
 */
public interface OrderService extends IService<Order> {


    CommonResult removeOrder(Order o);

    CommonResult selectByPage(String user, int page, int limit);


    CommonResult editOrder(Order order);

    CommonResult addOrder(Order order);

    CommonResult getByUserId(String userId);
}
