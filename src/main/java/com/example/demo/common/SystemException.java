package com.example.demo.common;

/**
 * 自定义异常处理类
 * @author Administrator
 * @date 2022年11月23日 21点22分
 */

public class SystemException extends RuntimeException {
    private Integer errorCode;
    private String errorMsg;

    public SystemException(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public SystemException() {
    }

    public SystemException(String message) {
        super(message);
    }
}
