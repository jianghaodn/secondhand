package com.example.demo.common;

import com.example.demo.pojo.common.CommonResult;
import com.example.demo.utils.WebUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理类
 *
 * @author Administrator
 */

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({SystemException.class})
    public void handleSysException(HttpServletResponse response,
                                   SystemException e) {
        log.error("发生了系统异常");
        e.printStackTrace();
        //向前端直接返回异常信息
        WebUtils.render(response, CommonResult.error(e.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletResponse response,
                                Exception e) {
        log.error("发生了未处理的异常");
        e.printStackTrace();
        //向前端直接返回异常信息
//        WebUtils.render(response, CommonResult.error("未知异常"));
        WebUtils.render(response,CommonResult.result(e));
    }
}
