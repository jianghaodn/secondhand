package com.example.demo.pojo.DO;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @author Administrator
 * @TableName goods
 */
@TableName(value = "goods")
@Data
@ApiModel("商品实体类")
public class Goods implements Serializable {
    /**
     * 商品编号
     */
    @TableId(value = "p_id")
    @ApiModelProperty("商品id")
    private String id;

    /**
     * 商品名称
     */
    @TableField(value = "p_name")
    @ApiModelProperty("商品名称")
    private String name;

    /**
     * 商品价格
     */
    @TableField(value = "p_price")
    @ApiModelProperty("商品价格")
    private Double price;

    /**
     * 商品描述
     */
    @TableField(value = "p_description")
    @ApiModelProperty("商品描述")
    private String description;

    /**
     * 商品图片
     */
    @TableField(value = "p_picture")
    @ApiModelProperty("商品图片")
    private String picture;

    /**
     * 商品标题
     */
    @TableField(value = "p_title")
    @ApiModelProperty("商品标题")
    private String title;

    /**
     * 商品创建时间
     */
    @ApiModelProperty("商品创建时间")
    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    private Date gmtCreate;

    /**
     * 商品修改时间
     */
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("商品更新时间")
    private Date gmtModified;

    /**
     * 商品数量
     */
    @TableField(value = "p_num")
    @ApiModelProperty("商品数量")
    private Integer num;

    /**
     * 1表示删除，0表示未删除
     */
    @TableField(value = "is_deleted")
    @ApiModelProperty("逻辑删除字段")
    private Boolean isDeleted;

    /**
     * 商品所属者id
     */
    @TableField(value = "p_user_id")
    @ApiModelProperty("商品所属用户id")
    private String userId;

    /**
     * 商品分类id
     */
    @TableField(value = "p_category_id")
    @ApiModelProperty("商品所属种类id")
    private String categoryId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}