package com.example.demo.pojo.DO;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 保存用户密钥
 * @TableName user_encrypt
 */
@TableName(value ="user_encrypt")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserEncrypt implements Serializable {
    /**
     * 用户名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 用户密钥
     */
    @TableField(value = "encrypt")
    private String encrypt;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}