package com.example.demo.pojo.DO;


import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @TableName category
 */
@TableName(value = "category")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category implements Serializable {
    /**
     * 分类编号
     */
    @TableId("c_id")
    private String id;

    /**
     * 分类名称
     */
    @TableField("c_name")
    private String name;

    @TableField("c_status")
    private Boolean status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @TableLogic
    @TableField("is_deleted")
    private Integer deleted;

    public Category(String id) {
        this.id = id;
    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}