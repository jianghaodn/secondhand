package com.example.demo.pojo.DO;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @TableName shopping_car
 */
@TableName(value = "shopping_car")
@Data
public class ShoppingCar implements Serializable {
    /**
     * 购物车编号
     */
    @TableId("s_id")
    private String id;

    /**
     * 所属用户的用户名
     */
    @TableField("s_user_name")
    private String userName;

    /**
     * 所属商品编号
     */
    @TableField("s_goods_id")
    private String goodsId;

    /**
     * 购物车状态,0为禁用，1为正常
     */
    @TableField("s_status")
    private Boolean status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    @TableField("is_deleted")
    @TableLogic
    private Boolean deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}