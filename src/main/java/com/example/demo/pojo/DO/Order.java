package com.example.demo.pojo.DO;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * @author Administrator
 * @TableName order
 */
@TableName(value = "`order`")
@Data
public class Order implements Serializable {
    /**
     * 订单id
     */
    @TableId(value = "o_id")
    private String id;

    /**
     * 状态
     */
    @TableField(value = "o_status")
    private Boolean status;

    /**
     * 地址
     */
    @TableField(value = "o_address")
    private String address;

    /**
     * 买家id
     */
    @TableField(value = "o_buyer_name")
    private String buyerName;

    @TableField(value = "o_num")
    @ApiModelProperty(value = "交易物品的数量")
    private Integer num;

    /**
     * 卖家id
     */
    @TableField(value = "o_seller_name")
    private String sellerName;

    /**
     * 商品id
     */
    @TableField(value = "o_goods_id")
    private String goodsId;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;

    /**
     * 是否被删除，0代表否，1代表是（逻辑删除）
     */
    @TableField(value = "is_deleted")
    private Integer isDeleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}