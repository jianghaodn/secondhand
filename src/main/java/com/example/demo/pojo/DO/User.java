package com.example.demo.pojo.DO;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @TableName user
 */
@TableName(value = "user")
@Data
@ApiModel("用户实体类")
public class User implements Serializable {
    /**
     * 用户名
     */
    @TableId("u_id")
    @ApiModelProperty("用户id")
    @ExcelProperty("用户编号")
    private String id;

    /**
     * 用户密码
     */
    @TableField("u_password")
    @ApiModelProperty("用户密码")
    @ExcelProperty("用户密码")

    private String password;

    /**
     * 用户昵称
     */
    @TableField("u_nickname")
    @ApiModelProperty("用户昵称")
    @ExcelProperty("用户昵称")

    private String nickname;

    /**
     * 用户简介
     */
    @TableField("u_description")
    @ApiModelProperty("用户简介")
    @ExcelProperty("用户简介")

    private String description;

    /**
     * 用户头像
     */
    @TableField("u_avatar")
    @ApiModelProperty("用户头像")
    @ExcelProperty("用户头像")

    private String avatar;

    /**
     * 用户地址
     */
    @TableField("u_address")
    @ApiModelProperty("用户地址")
    @ExcelProperty("用户地址")

    private String address;

    /**
     * 用户电话
     */
    @TableField("u_phone")
    @ApiModelProperty("用户电话")
    @ExcelProperty("用户电话")

    private String phone;

    /**
     * 用户状态
     */
    @TableField("u_status")
    @TableLogic
    @ApiModelProperty("用户状态")
    @ExcelProperty("用户状态")

    private Boolean status;

    /**
     * 用户邮件
     */
    @TableField("u_email")
    @ApiModelProperty("用户邮箱")
    @ExcelProperty("用户邮箱")

    private String email;

    /**
     * 用户性别
     */
    @ApiModelProperty("用户性别")
    @TableField("u_gender")
    @ExcelProperty("用户性别")

    private Boolean gender;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    @ApiModelProperty("username，仅限测试，不能使用")
    @ExcelIgnore
    private String username;

    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    @ExcelProperty("创建时间")

    private Date gmtCreate;
    @TableField(value = "gmt_modify", fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty("修改时间")
    @ExcelProperty("修改时间")

    private Date gmtModify;

    @ApiModelProperty("上次登录时间")
    @TableField(value = "u_last_login")
    @ExcelProperty("上次登录时间")

    private Date lastLogin;

    @TableField("is_admin")
    @ApiModelProperty(value = "是否是管理员")
    @ExcelProperty("是否是管理员")

    private Boolean admin;
}