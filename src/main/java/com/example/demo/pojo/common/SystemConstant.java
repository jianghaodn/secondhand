package com.example.demo.pojo.common;

import lombok.Data;

/**
 * @author Administrator
 */
@Data
public class SystemConstant {
    public static final String USER_ID="userId";

    /**
     * 最大密码长度
     */
    public static final int PASSWORD_MAX_LENGTH = 50;

    /**
     * 最小密码长度
     */
    public static final int PASSWORD_MIN_LENGTH = 6;
    /**
     * 最大昵称长度
     */
    public static final int NICKNAME_MAX_LENGTH = 30;
    /**
     * 检查字符串是否匹配的正则表达式
     */
    public static final String CHECK_STR_REG = "^[`~!@#$%\\^&&+&*()-=+/?<>]$";

    public static final int GOODS_NAME_MAX_LENGTH = 255;
    public static final String ENCRYPT_SPLIT = "=";
    public static final String ADMIN_LOGIN = "/admin/Shimba/pages-login.html";
    public static final String INDEX = "/admin/Shimba/user-datatable.html";
}
