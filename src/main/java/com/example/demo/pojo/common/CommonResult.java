package com.example.demo.pojo.common;

import cn.hutool.core.util.ArrayUtil;
import com.example.demo.pojo.DO.Goods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

/**
 * @author Administrator
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> {
    private Integer code;
    private String msg;
    private T data;

    public CommonResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public CommonResult(T data) {
        this.data = data;
    }

    public static <T> CommonResult result(Integer code, String msg, T t) {
        return new CommonResult<>(code, msg, t);
    }

    public static <T> CommonResult result(Integer code, String msg) {
        return new CommonResult<>(code, msg);
    }

    public static <T> CommonResult result(T t) {
        return new CommonResult<>(t);
    }
    public static <T> CommonResult result(String msg) {
        return new CommonResult<>(200,msg);
    }

    public static <T> CommonResult okResult() {
        return new CommonResult<>(200, "操作成功");
    }

    public static <T> CommonResult okResult(T t) {
        return new CommonResult<>(200, "操作成功", t == null ? new ArrayList<Goods>() : t);
    }

    public static CommonResult error() {
        return new CommonResult(400, "操作失败");
    }

    public static CommonResult error(String msg) {
        return new CommonResult(400, msg);
    }

    public static CommonResult success() {
        return okResult(null);
    }
}
