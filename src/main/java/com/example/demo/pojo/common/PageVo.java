package com.example.demo.pojo.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 分页对象
 * @author Administrator
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVo<T> {

    /**
     * 当前数据库具有的总条数
     */
    private Long total;

    /**
     * 查询到需要返回给前端的数据
     */
    private T data;
}
