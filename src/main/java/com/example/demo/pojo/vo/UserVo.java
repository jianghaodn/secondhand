package com.example.demo.pojo.vo;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@ApiModel("用户视图对象")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserVo {
    /**
     * 用户名
     */
    @ApiModelProperty("用户id")
    private String id;

    /**
     * 用户昵称
     */
    @ApiModelProperty("用户昵称")
    private String nickname;

    /**
     * 用户简介
     */
    @ApiModelProperty("用户简介")
    private String description;

    /**
     * 用户头像
     */
    @ApiModelProperty("用户头像")
    private String avatar;

    /**
     * 用户地址
     */
    @ApiModelProperty("用户地址")
    private String address;

    /**
     * 用户电话
     */
    @ApiModelProperty("用户电话")
    private String phone;

    /**
     * 用户状态
     */
    @ApiModelProperty("用户状态")
    private Boolean status;

    /**
     * 用户邮件
     */
    @ApiModelProperty("用户邮箱")
    private String email;

    /**
     * 用户性别
     */
    @ApiModelProperty("用户性别")
    private Boolean gender;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("上次登录时间")
    @TableField(value = "u_last_login")
    private Date lastLogin;
}
