FROM openjdk:8

COPY /target/*.jar /usr/local/app.jar

ENV MYPATH /usr/local

EXPOSE 80

WORKDIR $MYPATH

#ENTRYPOINT ["nohup","java","-jar","/usr/local/app.jar",">","/usr/local/log/log.file","2>&1","&"]
ENTRYPOINT ["java","-jar","/usr/local/app.jar","&"]